# dabet ⏲️

## Name explanation

**dabet** is an acronym for **d**ur**a**tion **be**tween **t**imes.

## Description

It prints the duration between two times in seconds with an `s` to indicate the unit. You could for example use it to look up the duration between your birthday and now.

## Usage examples

- Print the time from 2022-02-19T19:00:00+01:00 to now in days  
  ```bash
  didu -u s -v $(dabet -n -s 2022-02-19T19:00:00+01:00 -e $(date --iso-8601=s)) -o d
  ```

## API

This program uses [Semantic Versioning](https://semver.org/). Therefore an API has to be defined:
The API or the means of interacting with this program are the the command line options and the command line output. Excluded are name, version and about in the `--help`.

If units, formats or options are removed a major version will be released. However if units, formats or options are added then that is considered backwards compatible and a major version will not be released.

So if changes are made to the exceptions then there will be no major or minor release because they are not part of the API. Thus if your program interacts with this one and uses features outside of the API you may expect them to break in between major releases.

## License

©️ 2022 Anna Aurora <mailto:anna@annaaurora.eu> <https://matrix.to/#/@papojari:artemislena.eu> <https://annaaurora.eu>

This program is free software: you can redistribute it and/or modify it under the terms of the GNU Lesser General Public License version 3 as published by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU Lesser General Public License version 3 along with this program. If not, see <https://www.gnu.org/licenses/>.

